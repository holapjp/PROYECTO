json.extract! permiso, :id, :crear, :actualizar, :eliminar, :consultar, :rol_id, :created_at, :updated_at
json.url permiso_url(permiso, format: :json)
