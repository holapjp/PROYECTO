class Rol < ApplicationRecord
     has_many :permisos, :dependent => :destroy
   accepts_nested_attributes_for :permisos, allow_destroy: true

   def permisos_for_form
         collection = permisos.where(rol_id: id)
         collection.any? ? collection : permisos.build
   end

    
    
end
