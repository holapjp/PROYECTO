class CreatePermisos < ActiveRecord::Migration[5.1]
  def change
    create_table :permisos do |t|
      t.boolean :crear
      t.boolean :actualizar
      t.boolean :eliminar
      t.boolean :consultar
      t.references :rol, foreign_key: true

      t.timestamps
    end
  end
end
